<?php
		session_start();
		unset($_SESSION['id']);
		unset($_SESSION['name']);
		unset($_SESSION['permission']);
		session_destroy();
		header("Location: userinfo.php");
?>